import { Component, OnInit } from '@angular/core';
import {UsersService} from './users.service';
@Component({
  selector: 'jce-users',
  templateUrl: './users.component.html',
  styles: [`
    .users li { cursor: default; }
      .users li:hover { background: #ecf0f1; }
    .list-group-item.active, 
    .list-group-item.active:hover { 
         background-color: #ecf0f1;
         border-color: #ecf0f1; 
         color: #2c3e50;
    } 
  `]
})
export class UsersComponent implements OnInit {
isloadnig:Boolean = true;
users;
currentUser;

select(user){
  this.currentUser = user
}

addUser(user){
  this._usersService.addUser(user); // push the data to the array 
}

updateUser(user){

  this._usersService.updateUser(user);
  // this.users.splice(
  //   this.users.indexOf(originalAndNew[0],1,originalAndNew[1])
  // )
  //   console.log(this.users);
}


  constructor(private _usersService:UsersService) { }
deleteUser(user){
  this._usersService.deleteUser(user);
  
}

  ngOnInit() {  
    // left usersData its the input of the function.
  //  this._usersService.getUsers().subscribe(usersData => 
  //  {this.users = usersData;
  //     this.isloadnig = false;
  // console.log(this.users)  
  // });
      
 this._usersService.getUsersFromApi().subscribe(usersData => 
   {this.users = usersData.result;
      this.isloadnig = false;
  console.log(this.users)  
  });
      
  }
}
