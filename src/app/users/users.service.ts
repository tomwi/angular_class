import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import {AngularFire} from 'angularfire2';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';


@Injectable()
export class UsersService {
private _url = 'http://tomwi.myweb.jce.ac.il';
usersObservable;


// private _url = 'http://jsonplaceholder.typicode.com/users' ;


getUsers(){
//reading data from firebase //// to read spacific data  from db we can refer it to /users/1  // same as sql query
this.usersObservable = this.af.database.list('/users');
return this.usersObservable;
// return this._http.get(this._url).map( res => res.json()).delay(2000)
}

getUsersFromApi(){
  //http protocol that talks with php server gets the data from the url and convert it to json
  return this._http.get(this._url).map(res => res.json());
}


addUser(user){
  this.usersObservable.push(user);

}
updateUser(user){ 
  let userKey = user.$key;
  let userData = {name:user.name, email:user.email};
  this.af.database.object('/users/' + userKey).update(userData); // update /users/1 update with the new data

}

deleteUser(user){
    let userKey = user.$key;
     this.af.database.object('/users/' + userKey).remove();

}
// dealering object from type angular fire
  constructor(private af:AngularFire, private _http:Http) { }

}
   


