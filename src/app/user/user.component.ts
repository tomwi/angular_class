import { Component, OnInit, EventEmitter, Output } from '@angular/core'; 
import {User} from './user'

@Component({
  selector: 'jce-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
  inputs:['user',]
})
export class UserComponent implements OnInit {
user:User;
@Output() deleteEvent = new EventEmitter<User>(); 
@Output() editEvent = new EventEmitter<User>();

  isEdit:Boolean = false;
  editButtonText = "Edit";
  tempUser:User = {email:null,name:null};
  constructor() { }
sendDelete(){ // user תפלוט ותעביר את המידע ל 
  this.deleteEvent.emit(this.user);

}
  toggleEdit(){
    this.isEdit = !this.isEdit;
    this.isEdit ? this.editButtonText = 'save' : this.editButtonText = 'Edit' 
    if(!this.isEdit){
        this.editEvent.emit(this.user);
        // this.tempUser.email = this.user.email
        // this.tempUser.name = this.user.name
    }else {
      let originalAndNew = [];
      originalAndNew.push(this.tempUser, this.user);
      // this.editEvent.emit(originalAndNew);

    }
  }

  cancelEdit(){
    this.isEdit = false;
    this.user.email = this.tempUser.email;
    this.user.name = this.tempUser.name;

  }
  ngOnInit() {
  }

}
