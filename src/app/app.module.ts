import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {RouterModule,Routes} from '@angular/router';
import {AngularFireModule} from 'angularfire2';

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { UsersService } from './users/users.service';
import { UserComponent } from './user/user.component';
import { SpinnerComponent } from './shared/spinner/spinner.component';
import {PageNotFoundComponent} from './Page-not-found/Page-not-found.component'; 
import {PostsComponent} from './posts/posts.component';
import {UserFormComponent} from './user-form/user-form.component';

export const fireBaseConfig = {
      apiKey: "AIzaSyAYlQmTfOk2nr8ZM3yi0P0mTsJ9yxc3r3s",
    authDomain: "angular-project-897d6.firebaseapp.com",
    databaseURL: "https://angular-project-897d6.firebaseio.com",
    storageBucket: "angular-project-897d6.appspot.com",
    messagingSenderId: "682458816912"
}



//load relavent components based url
const appRoutes:Routes = [
{path:'users', component:UsersComponent},
{path:'posts', component:PostsComponent},
{path:'', component:UsersComponent},
//url not exist **
{path:'**', component:PageNotFoundComponent}

]


@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    UserComponent,
    SpinnerComponent,
    PostsComponent,
    PageNotFoundComponent,
    UserFormComponent
  ],

  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(fireBaseConfig)
  ],
  providers: [UsersService],
  bootstrap: [AppComponent]
})
export class AppModule { }
